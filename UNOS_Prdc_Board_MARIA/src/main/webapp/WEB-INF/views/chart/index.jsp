<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<style type="text/css">
	.selected_span{
	background-color :white;
	color : #008900;
}
	
</style>
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>

<style>

/* @font-face {font-family:MalgunGothic; src:url(../fonts/malgun.ttf);}

*{
	font-family:'MalgunGothic';
} */

#container{
	background-color: black;
}

body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow: hidden;
	background-color: black;
  	font-family:'Helvetica';
}

.menu:HOVER{
	background-color: white;
	color: gray;
}
.selected_menu{
	background-color :rgb(33,128,250);
	color : white;
}

.unSelected_menu{
	background-color :gray;
	color : white
}

</style>
<script type="text/javascript">

	$(window).resize(function() {
		location.reload();
	});
	
	var line = "${_line}";
	if(line=="") line = "ALL";
	var shopId = 1;
	var panel = false;
	
	
	function showPanel(){
		$("#panel").animate({
			"left" : 0
		});
		
		$("#menu_btn").animate({
			"left" : contentWidth * 0.2 + getElSize(50)	
		});
		
		$("#corver").css({
			"z-index" : 9,
			"opacity" : 0.7
		});
	};

	function closePanel(){
		$("#panel").animate({
			"left" : -contentWidth * 0.2 - getElSize(40)
		});
		
		$("#menu_btn").animate({
			"left" : 0
		});
		
		$("#corver").css({
			"z-index" : -1,
			"opacity" : 0.7
		});
	};
	
	$(function(){
		$("#logout, #id_font, #appVer").remove()
		setDate();
		$("#menu_btn").click(function(){
			location.href = "/iDOO_CONTROL/chart/main.do";
		});
		
		setEl();
		getDvcSelId();
// 		getDvcId(); // 사용안함 
		
		getCirTime();
		
// 		interval = setInterval(getDvcId, 5000);
		
		$("#panel_table td").addClass("unSelected_menu");
		$("#menu11").removeClass("unSelected_menu");
		$("#menu11").addClass("selected_menu");
	});
	
	var cirTime = 0;
	
	function getCirTime(){
		var url = ctxPath + "/getCirTime.do";
		
		$.ajax({
			url : url,
			dataType : "json",
			async : false,
			type : "post",
			success : function(data){
				cirTime = data;
				console.log("cirTime: " + cirTime);
				
				interval = setInterval(getDvcSelId, cirTime * 1000);
			}
		});
	};
	
	var dvcArray = [];
	var alarmDvcArray =[];
// 	var dvcSelArray = [];
	var dvcIdx = 0;
	var alarmIdx = 0;
// 	var dur = 0;
	
	function getDvcSelId(){

		var url = ctxPath + "/getDvcSelId.do";
		
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var today = year + "-" + month + "-" + day;
		var param = "&Date=" + today;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			async : false,
			type : "post",
			success : function(data){
				
				//console.log("data.length : " + data.dataList.length);
								
				if(data.dataList.length != 0){
					//console.log("doGet2");
					doGetData(data);
				}else{
					console.log("데이터 없음");
					$("#dvcName").html("-");
					$("#targetCnt").html(0).css("color","red");
					$("#currentCnt").html(0).css("color","red");
					$("#targetRatio").html(0).css("color","red");
					$("#alarm").html("");
				}
				
				/* else {
					console.log("getDvcId");
					getDvcId();
				} */
			}
		});
	};
	
	function getDvcId(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		
		//var url = ctxPath + "/chart/getDvcNameList.do";
		var url = ctxPath + "/getDvcIdForSign.do";
		var param = "shopId=" + shopId + 
					"&line=" + line + 
					"&sDate=" + today + 
					"&eDate=" + today + " 23:59:59" +
					"&type=" + tagetType;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			async : false,
			type : "post",
			success : function(data){
				doGetData(data);
			}
		});
	};
	
	function doGetData(data){
		var json = data.dataList;
					
		dvcArray = [];
		alarmDvcArray = [];
		
		$(json).each(function(idx, data){
			if(data.status=="ALARM"){
				alarmDvcArray.push(data.id);
			}else{
				dvcArray.push(data.id);
			}
		});
		
// 		dvcArray = [];
// 		dvcArray.push("3");
// 		dvcArray.push("5");
		
		//console.log(dvcArray);

		//interval = setTimeout(getDvcId, dur)
		
		if(alarmDvcArray.length==0){
			alarmIdx = 0;
			getData(dvcArray[dvcIdx]);
			
			dvcIdx++;
			if(dvcIdx>=dvcArray.length) dvcIdx = 0;
			//dur = 1000 * 5;
		}else{
			getData(alarmDvcArray[alarmIdx]);
			alarmIdx++;
			if(alarmIdx>=alarmDvcArray.length) alarmIdx = 0;
			//dur = 1000 * 20;
		}
		
		//console.log(new Date().getTime())
		//getData(dvcArray[0]);
		
		skip = true;
	};
	
	var interval = null;
	var idx = 1;
	var alarm_interval = null;
	function alarmAnim(){
		alarm_interval = setInterval(function(){
			if($("#container").css("background-color")=="rgb(0, 0, 0)"){
				$("#container").css("background-color", "rgb(255, 0, 0)");
			}else{
				$("#container").css("background-color", "rgb(0, 0, 0)");
			}
		}, 500);
	};
	
	var skip = false;
	
	function getData(dvcId){
		clearInterval(alarm_interval);
		$("#container").css("background-color", "rgb(0, 0, 0)");
		
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var today = year + "-" + month + "-" + day;
		
		var url = ctxPath + "/getData.do";
		var param = "dvcId=" + dvcId +
					"&sDate=" + today +
					"&eDate=" + today + " 23:59:59" + 
					"&type=" + tagetType;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				
				console.log("dvcId : " + dvcId);
				
				// if there is no targetRatio, it present nothing by under the code, but it is run by 'getDvcSelId' select statement so annotation 
				/* if(data.targetRatio==0 || data.targetRatio>100 && skip){
					skip = false;
					clearInterval(interval);
					console.log("CLEAR TIMEOUT")
					interval = setInterval(getDvcSelId, cirTime * 1000);
					//getCirTime();
					return;
				}; */
				
				// wilson test ----------
								
				/* if(data.targetRatio==0 || data.targetRatio>100 && skip){
					
					if(data.name == "PUMA300LC"){
						data.status = "ALARM";
					}else{
						data.status = "WAIT";
					}
					
					data.tgCnt = 10
					data.cntCyl = 9
					data.targetRatio = 90
				} */
								
				// end test -------------
				
				//console.log(data)
				var dvcName = data.name;
				$("#alarmPop").css("z-index",-9);
				
				var alarm = "<br>";
				var color;
				
				var status = "";
				if(data.status=="IN-CYCLE"){
					alarm = "${incycle_board}";
					color = "#329C00";
				}else if(data.status=="WAIT"){
					alarm = "${wait_board}";
					color = "yellow";
				}else if(data.status=="ALARM"){
					var alarm_msg = data.lastAlarmCode + "-" + data.lastAlarmMsg;
					if(alarm_msg=="-"){
						alarm_msg = "[장비 확인 필요]";
					};
					
					color = "yellow";
					alarmAnim();
					$("#alarmPop").html(alarm_msg).css({
						"z-index" : 1,
						"font-size" : getElSize(200),
						"border" : getElSize(10) + "px solid yellow"
					});
					
					$("#alarmPop").css({
						"top" : (window.innerHeight/2) - ($("#alarmPop").height()/2),
						"left" : (window.innerWidth/2) - ($("#alarmPop").width()/2),
						"padding" : getElSize(50)
					});
					
				}else if(data.status=="NO-CONNECTION"){
					alarm = "${noconnection_board}";
					color = "gray";
				}
				
				var opRatioColor = "";
				if(data.targetRatio>=95){
					opRatioColor = "#329C00";
				}else{
					opRatioColor = "red";
				}
				

				$("#dvcName").html(dvcName);
				$("#targetCnt").html(data.tgCnt).css("color","white");
				$("#currentCnt").html(data.cntCyl).css("color",opRatioColor);
				$("#targetRatio").html(data.targetRatio + "%").css("color",opRatioColor);
				$("#alarm").html(alarm).css({
					"color" : color,
					"font-weight" : "bolder"
				});
				
				
				$(".machineListForTarget #machineListTable td input").css("color","black");
			},
			error : function(e1,e2,e3){
				console.log(e1,e2,e3)
			}
		});
	};
	
	function addZero(str){
		if(str.length==1){
			str = "0" + str;
		};
		return str;
	};
	
	var tagetType;
	function setDate(){
		setTimeout(setDate, 1000);
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		
		var today = year + ". " + month + ". " + day + " " + hour + ":" + minute; 
		var type;
		if(((Number(hour) * 60) + Number(minute) >= 510) && ((Number(hour) * 60) + Number(minute) <= 1230)){
			type = "${day}";
			tagetType = 2;
		}else{
			type = "${night}";
			tagetType = 1;
		};
		
//		$("#testDrive").html(today + " " + type).css({
		$("#testDrive").html(today).css({
			"font-size" : getElSize(200) + "px",
			"font-weight" : "bolder"
		});
		
		$(".neon").css({
			"font-size" : getElSize(220) + "px",
			//"text-shadow" : "0 0 " + getElSize(20) + "px #fff, 0 0 " + getElSize(30) + "px #fff, 0 0 " + getElSize(20) + "px #fff, 0 0 " + getElSize(20) + "px #ff0000, 0 0 " +getElSize(20) + "px #ff0000, 0 0 " + getElSize(120) + "px #ff0000, 0 0 " +getElSize(120) + "px #ff0000, 0 0 " + getElSize(150) + "px #ff0000",
			"text-shadow" :  getElSize(-10) + "px 0 black, 0 " + getElSize(10) + "px black, " + getElSize(10) + "px 0 black, 0 " + getElSize(-10) + "px black",
 
			"margin-top" : getElSize(60),
		});
		
		$("#testDrive").css({
			//"text-shadow" : "0 0 " + getElSize(20) + "px #fff, 0 0 " + getElSize(30) + "px #fff, 0 0 " + getElSize(20) + "px #fff, 0 0 " + getElSize(20) + "px #ff0000, 0 0 " +getElSize(20) + "px #ff0000, 0 0 " + getElSize(120) + "px #ff0000, 0 0 " +getElSize(120) + "px #ff0000, 0 0 " + getElSize(150) + "px #ff0000",
		})
		
		$("#alarm").css({
			"text-shadow" :  getElSize(-10) + "px 0 black, 0 " + getElSize(10) + "px black, " + getElSize(10) + "px 0 black, 0 " + getElSize(-10) + "px black",
			"font-size" : getElSize(130),
			"font-weight" : "bolder"
		});
	};
	
	function setEl(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#container").css({
			"width" : originWidth,
			"height" : originHeight,
		});
		
		$(".title, #dvcName").css({
			"font-size" : getElSize(240) + "px",
			"color" : "white",
			"font-weight" : "bolder"
		});
		
		$(".div").css({
			"margin-top" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$(".neon").css({
			"float" : "right",
			"color" : "red",
			"margin-right" : getElSize(400)
		});
		
		$("#testDrive").css({
			"color" : "yellow"
		});
		
		$(".title").css({
			"float" : "left",
			"margin-left" : getElSize(400)
		});
		
		$("#targetRatio").css({
			"margin-left" : -getElSize(550)
		});
		
		$("#mainTable").css({
			"border-radius" : getElSize(20),
			"height" : originHeight
		});
		
		$("#mainTable td").css({
			"padding" : getElSize(25)
		});
		
		
		$("#mainTable td").css({
			"border" : getElSize(10) + "px solid yellow"
		});
		
		
		$("#flagDiv").css({
			"top": getElSize(10),
			"left": getElSize(10)
		});

		$("#panel").css({
			"background-color" : "rgb(34,34,34)",
			"border" : getElSize(20) + "px solid rgb(50,50,50)",
			"position" : "absolute",
			"width" : contentWidth * 0.2,
			"top" : 0,
			"left" : -contentWidth * 0.2 - getElSize(40),
			"z-index" : 99999
		});
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(40)
		});

		$("#panel_table td").css({
			"padding" : getElSize(30),
			"cursor" : "pointer"
		});

		$("#panel_table td").addClass("unSelected_menu");
		
		$(".machineListForTarget").css({
			"position" : "absolute",
			"width" : getElSize(1200),
			"height" : getElSize(1200),
			"overflow" : "auto",
			//"top" : getElSize(50),
			//"background-color" : "rgb(34,34,34)",
			"background-color" : "green",
			"color" : "white",
			"font-size" : getElSize(50),
			"padding" : getElSize(50),
			"overflow" : "auto",
			"border-radius" : getElSize(50),
			"border" : getElSize(10) + "px solid white",
		});
		
		$(".machineListForTarget").css({
			"left" : (originWidth/2) - ($(".machineListForTarget").width()/2),
		});
		
		$("#bannerDiv").css({
			"position" : "absolute",
			"width" : getElSize(1500),
			"height" : getElSize(200),
			"border-radius" : getElSize(20),
			"padding" : getElSize(50),
			"background-color" : "lightgray",
			"z-index" : -9
		});

		
		$("#bannerDiv").css({
			"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
			"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
		});
		
		$("#bannerDiv input[type='text']").css({
			"width" : getElSize(1200),
			"font-size" : getElSize(50)
		});
		
		$("#bannerDiv button").css({
			"margin" : getElSize(50),
			"font-size" : getElSize(50)
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.3,
			"position" : "absolute",
			//"background-color" : "white",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(100),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		
		$(".machineListForTarget #machineListTable td input").css("color","black");
		chkBanner();
		
		$("#alarmPop").css({
			"z-index" : -99,
			"position" : "absolute",
			"height" : getElSize(300),
			"background-color" : "white"
		});
		
		$("#menu_btn").css({
			"position" : "absolute",
			"width" : getElSize(130),
			"left" : getElSize(10),
			"top" : getElSize(120),
			"cursor" : "pointer",
			"z-index" : 13
		});

	};
</script>
</head>
<body>
	<div id="machineListForTarget_day" style="opacity:0" class="machineListForTarget">
		<center>
			<table id="machineListTable" style="width: 100%">
			</table>
		</center>
	</div>
	<div id="machineListForTarget_night" style="opacity:0" class="machineListForTarget">
		<center>
			<table id="machineListTable" style="width: 100%">
			</table>
		</center>
	</div>	
	
	<div id="bannerDiv">
		<Center>
			<input type="text" id="banner" size="100"><input type="color" id="color"><br>
			<button onclick="addBanner();">적용</button>
			<button onclick="cancelBanner();">미적용</button>
		</Center>
	</div>
	<div id="intro_back"></div>
	<span id="intro"></span>
	
	<div id="container">
	<img src="${ctxPath }/images/home.png" id="menu_btn" >
		<div id="alarmPop">
		</div>
		<div id="wrapper">
			<center>
				<table style="width: 100%; border-collapse: collapse;" id="mainTable">
					<Tr>
						<Td style="text-align: center;">
							<div id="testDrive" class="icon- testDrive"></div>
							<span id="dvcName"> - </span>
						</Td>
					</Tr>
					<Tr>
						<Td style="text-align: center;vertical-align: middle;"><span class="title"><spring:message code="target_cnt"></spring:message></span><span class="icon- neon" id="targetCnt">0</span> </Td>
					</Tr>
					<Tr>
						<Td style="text-align: center;vertical-align: middle;"><span class="title" ><spring:message code="prd_cnt"></spring:message></span> <span class="icon- neon" id="currentCnt">0</span></Td>
					</Tr>
					<Tr>
						<Td style="text-align: center;vertical-align: middle;"><span class="title" ><spring:message code="target_ratio"></spring:message></span> <span class="icon- neon" id="targetRatio">0</span></Td>
					</Tr>
					<Tr>
						<Td style="text-align: center;"><span  id="alarm">-</span></Td>
					</Tr>
				</table>
			</center>
		</div>
	</div>
</body>
</html>