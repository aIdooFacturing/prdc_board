package com.unomic.dulink.chart.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.service.ChartService;
/**
 * Handles requests for the application home page.
 */
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/")
@Controller
public class ChartController {

	private static final Logger logger = LoggerFactory.getLogger(ChartController.class);
	final int shopId = 1;
	/**    
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private ChartService chartService; 
	
	
	@RequestMapping(value="index")
	public String alarmReport(){
		return "chart/index";
	};
	
	@RequestMapping(value = "getData")
	@ResponseBody
	public ChartVo getData(ChartVo chartVo) {
		try {
			chartVo = chartService.getData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	}
	
	@RequestMapping(value = "getDvcIdForSign")
	@ResponseBody
	public String getDvcIdForSign(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getDvcIdForSign(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value = "getDvcSelId")
	@ResponseBody
	public String getDvcSelId(ChartVo chartVo) {
		String str = "";
		try {
			str = chartService.getDvcSelId(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	
	//common func
	
	@RequestMapping(value="getAppList")
	@ResponseBody
	public String getAppList(ChartVo chartVo){
		String str = ""; 
		try {
			str = chartService.getAppList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return str;
	}
	
	
	@RequestMapping(value="removeApp")
	@ResponseBody
	public String removeApp(ChartVo chartVo){
		String str = ""; 
		try {
			str = chartService.removeApp(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return str;
	}
	
	float progress = 0;
	
	@RequestMapping(value = "resetProgress")
	@ResponseBody
	public void resetProgress(){
		progress = 0;
	};
	
	@RequestMapping(value = "getProgress")
	@ResponseBody
	public float getProgress(){
		//System.out.println(progress);
		return progress;
	};
	
	@RequestMapping(value = "fileDown")
	@ResponseBody
	public String sample(ChartVo chartVo) throws Exception{
		String fileLocation = chartVo.getFileLocation();
        String fileName = fileLocation.substring(fileLocation.lastIndexOf("/") + 1);
        
        File file = new File(System.getProperty( "catalina.base" ) + "/webapps/" + fileName);
        //File file = new File("/Users/jeongwan/Desktop/" + fileName);
        URL url = new URL(fileLocation);
        
        progress = 0;
        System.out.println(file);
        String str = "";
        try (FileOutputStream fos = new FileOutputStream(file)){
            HttpURLConnection httpConnection = (HttpURLConnection) (url.openConnection());
            float completeFileSize = httpConnection.getContentLength();
            
        	FileUtils.copyURLToFile(url, file);

        	BufferedInputStream in = new BufferedInputStream(httpConnection.getInputStream());
        	byte[] data = new byte[1024];
            float downloadedFileSize = 0;
            float x = 0;
            while ((x = in.read(data, 0, 1024)) >= 0) {
                downloadedFileSize += x;
                progress = (downloadedFileSize/completeFileSize)*100;
            }
            
            chartService.addNewApp(chartVo);
            in.close();
            
            str = "success";
        } catch (Exception e){
        	e.printStackTrace();
        	str = "fail";
        }
        
        return str;
	};
	
	
	
	@RequestMapping(value="login")
	@ResponseBody 
	public String loginCnt(ChartVo chartVo){
		String str = ""; 
		try {
			str = chartService.login(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return str;
	}
	
	
	@RequestMapping(value="getStartTime")
	@ResponseBody
	public String getStartTime(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getStartTime(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	
	@RequestMapping(value="getComName")
	@ResponseBody 
	public String getComName(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getComName(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getBanner")
	@ResponseBody
	public ChartVo getBanner(ChartVo chartVo){
		try {
			chartVo = chartService.getBanner(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return chartVo;
	}
	
	@RequestMapping(value="getCirTime")
	@ResponseBody 
	public String getCirTime(){
		String str = "";
		try {
			str = chartService.getCirTime();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
};

