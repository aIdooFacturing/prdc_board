
package com.unomic.dulink.chart.domain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;




@Getter
@Setter
@ToString
public class ChartVo{
	String status;
	String lastAlarmCode;
	String lastAlarmMsg;
	String tgCnt;
	String cntCyl;
	String targetRatio;
	String sDate;
	String eDate;
	String Date;
	
	//common
	String ty;
	String fileLocation;
	String url;
	String fileName;
	String appId;
	String appName;
	String categoryId;
	Integer shopId;
	String dvcId;
	String id;
	String pwd;
	String name;
	String msg;
	String rgb;
}
